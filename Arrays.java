import java.util.Random;

public class Arrays{
	public static void main(String[] args) {
		Random r = new Random();
		int arr[] = new int[8];
		for(int i = 0; i < 8; i++) {
			arr[i] = r.nextInt(1, 11);
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		boolean flag = true;
		for(int i = 1; i < 8; i++) {
			if(arr[i - 1] >= arr[i]) {
				flag = false;
				break;
			}
		}
		if(flag) {
			System.out.println("Массив является строго возрастающей последовательностью.");
		}else {
			System.out.println("Массив не является строго возрастающей последовательностью.");
		}
		for(int i = 1; i < 8; i+=2) {
			arr[i] = 0;
		}
		for(int i = 0; i < 8; i++) {
			System.out.print(arr[i] + " ");
		}
		
	}
}
